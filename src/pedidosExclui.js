let pedidosExclui = []

const loadPedidosExclui = function(){
    const pedidosExcluiJSON = sessionStorage.getItem('pedidosExclui')
    
    if(pedidosExcluiJSON !== null){
        return JSON.parse(pedidosExcluiJSON)
    } else {
        return []
    }
}

const savePedidosExclui = function(){
    sessionStorage.setItem('pedidosExclui', JSON.stringify(pedidosExclui))
}

//expose orders from module
const getPedidosExclui = () => pedidosExclui

const criaPedidosExclui = (select) =>{
    
    pedidosExclui.push({
        qtd: select
    })
    savePedidosExclui()
}

const removePedidosExclui = (item) => {
    pedidosExclui.splice(item, 1)
    savePedidosExclui()
}

const apagaPedidosExclui = () => {
    sessionStorage.removeItem('pedidosExclui')
}

pedidosExclui = loadPedidosExclui()

export { getPedidosExclui, criaPedidosExclui, savePedidosExclui, removePedidosExclui, apagaPedidosExclui}