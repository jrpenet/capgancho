import {insereClientes, removeClientes} from './cliente'
import {getPedidos} from './pedidos'
import {getCadastro, resetaCad, mudaDados2} from './cadastro'

//preenchimento dos dados caso solicite delivery

const delivery = () => {
   const anotaNome = document.createElement('p')
   anotaNome.textContent = 'Nome: '
   anotaNome.setAttribute('class', 'revisaDados')
   document.querySelector('#recebeEmCasa').appendChild(anotaNome)

   const mostraNome = document.createElement('span')
   mostraNome.textContent = getCadastro()[0].nome
   mostraNome.setAttribute('class', 'infoDados')
   document.querySelector('#recebeEmCasa').appendChild(mostraNome)

   const anotaCel = document.createElement('p')
   anotaCel.textContent = 'Celular: '
   anotaCel.setAttribute('class', 'revisaDados')
   document.querySelector('#recebeEmCasa').appendChild(anotaCel)

   const mostraCel = document.createElement('span')
   mostraCel.textContent = getCadastro()[0].cel
   mostraCel.setAttribute('class', 'infoDados')
   document.querySelector('#recebeEmCasa').appendChild(mostraCel)

   const anotaCep = document.createElement('p')
   anotaCep.textContent = 'CEP: '
   anotaCep.setAttribute('class', 'revisaDados')
   document.querySelector('#recebeEmCasa').appendChild(anotaCep)

   const mostraCEP = document.createElement('span')
   mostraCEP.textContent = getCadastro()[0].cep
   mostraCEP.setAttribute('class', 'infoDados')
   document.querySelector('#recebeEmCasa').appendChild(mostraCEP)

   const anotaEndereco = document.createElement('p')
   anotaEndereco.textContent = 'Endereço: '
   anotaEndereco.setAttribute('class', 'revisaDados')
   document.querySelector('#recebeEmCasa').appendChild(anotaEndereco)

   const mostraEndereco = document.createElement('span')
   mostraEndereco.textContent = getCadastro()[0].rua + ', ' + getCadastro()[0].n
   mostraEndereco.setAttribute('class', 'infoDados')
   document.querySelector('#recebeEmCasa').appendChild(mostraEndereco)

   const anotaPref = document.createElement('p')
   anotaPref.textContent = 'Ponto de referência: '
   anotaPref.setAttribute('class', 'revisaDados')
   document.querySelector('#recebeEmCasa').appendChild(anotaPref)

   const mostraPref = document.createElement('span')
   mostraPref.textContent = getCadastro()[0].pontoDeReferencia
   mostraPref.setAttribute('class', 'infoDados')
   document.querySelector('#recebeEmCasa').appendChild(mostraPref)

   const anotaBairro = document.createElement('p')
   anotaBairro.textContent = 'Bairro: '
   anotaBairro.setAttribute('class', 'revisaDados')
   document.querySelector('#recebeEmCasa').appendChild(anotaBairro)

   const mostraBairro = document.createElement('span')
   mostraBairro.textContent = getCadastro()[0].nomeDoBairro
   mostraBairro.setAttribute('class', 'infoDados')
   document.querySelector('#recebeEmCasa').appendChild(mostraBairro)

}

//preenchimento dos dados caso retire na loja

const retiraNoLocal = () => {
   const anotaNome = document.createElement('p')
   anotaNome.textContent = 'Nome: '
   anotaNome.setAttribute('class', 'revisaDados aproxima')
   document.querySelector('#recebeEmCasa').appendChild(anotaNome)

   const mostraNome = document.createElement('span')
   mostraNome.textContent = getCadastro()[0].nome
   mostraNome.setAttribute('class', 'revisaDados')
   document.querySelector('#recebeEmCasa').appendChild(mostraNome)

   const anotaCel = document.createElement('p')
   anotaCel.textContent = 'Celular: '
   anotaCel.setAttribute('class', 'revisaDados aproxima')
   document.querySelector('#recebeEmCasa').appendChild(anotaCel)

   const mostraCel = document.createElement('span')
   mostraCel.textContent = getCadastro()[0].cel
   mostraCel.setAttribute('class', 'revisaDados')
   document.querySelector('#recebeEmCasa').appendChild(mostraCel)

   const aviso = document.createElement('p')
   aviso.textContent = 'O pedido será retirado na loja'
   aviso.setAttribute('class', 'aviso')
   document.querySelector('#recebeEmCasa').appendChild(aviso)

}

if(getPedidos().filter((x) => x.tipo === 'taxa')[0].produto === 'Retirar na loja'){
   retiraNoLocal()
}else{
   delivery()
}

const enviaPedido = document.querySelector('#confiPedido')
enviaPedido.addEventListener('click', (e) => {
   e.preventDefault()

   const pgt = document.querySelector('#pagaEM').value
   const troco = document.querySelector('#troco').value
   const obs = document.querySelector('#observacao').value

   mudaDados2(pgt, troco, obs)
   location.assign('./enviapedidos.html')
})

const reset = document.querySelector('#mudacli')
reset.addEventListener('click', (e) => {
   e.preventDefault()
   resetaCad()
   location.assign('./home.html')
})
