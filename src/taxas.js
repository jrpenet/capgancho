import {criaPedidos, getPedidos} from './pedidos'

const pulaTaxa = () => {
    if(getPedidos().map((x) => x.taxa).includes('tipo')){
        location.assign('./confirma.html')
    }
}

const addTblEl = function (){
    const tblEl = document.createElement('table')
    document.querySelector('#txBairro').appendChild(tblEl)
    
    //header da tabela
    
    const qtyEl = document.createElement('th')
    qtyEl.textContent = ''
    document.querySelector('table').appendChild(qtyEl)
    
    const district = document.createElement('th')
    district.textContent = 'BAIRRO'
    document.querySelector('table').appendChild(district)
    
    const priceEl = document.createElement('th')
    priceEl.textContent = 'TAXA DE ENTREGA'
    document.querySelector('table').appendChild(priceEl)
    
    const solicitaTx = document.createElement('th')
    solicitaTx.textContent = 'PEDIR'
    document.querySelector('table').appendChild(solicitaTx)
}

//gera o menu na tabela

const bairrosLista = [{
    nomeBairro: 'Retirar na Moraes Lanches',
    valor: 0,
    taxa: 'taxa',
    formBairro: 'Retirar na Moraes Lanches'
}, {
    nomeBairro: 'UR-5',
    valor: 3,
    taxa: 'taxa',
    formBairro: 'UR-5'
},{
    nomeBairro: 'UR-4',
    valor: 3,
    taxa: 'taxa',
    formBairro: 'UR-4'
},{
    nomeBairro: 'Lagoa Encantada',
    valor: 3,
    taxa: 'taxa',
    formBairro: 'Lagoa Encantada'
},{
    nomeBairro: 'Jordão Alto',
    valor: 3,
    taxa: 'taxa',
    formBairro: 'Jordão Alto'
},{
    nomeBairro: 'UR-6',
    valor: 4,
    taxa: 'taxa',
    formBairro: 'UR-6'
},{
    nomeBairro: 'UR-11',
    valor: 4,
    taxa: 'taxa',
    formBairro: 'UR-11'
},{
    nomeBairro: 'UR-1',
    valor: 4,
    taxa: 'taxa',
    formBairro: 'UR-1'
},{
    nomeBairro: 'UR-2',
    valor: 4,
    taxa: 'taxa',
    formBairro: 'UR-2'
},{
    nomeBairro: 'UR-3',
    valor: 4,
    taxa: 'taxa',
    formBairro: 'UR-3'
},,{
    nomeBairro: 'Zumbi do Pacheco',
    valor: 4,
    taxa: 'taxa',
    formBairro: 'Zumbi do Pacheco'
},{
    nomeBairro: 'Dois Carneiros',
    valor: 4,
    taxa: 'taxa',
    formBairro: 'Dois Carneiros'
},{
    nomeBairro: 'Ibura de Baixo',
    valor: 5,
    taxa: 'taxa',
    formBairro: 'Ibura de Baixo'
},{
    nomeBairro: 'Jordão Baixo',
    valor: 5,
    taxa: 'taxa',
    formBairro: 'Jordão Baixo'
},{
    nomeBairro: 'Jardim Jordão Residencial',
    valor: 5,
    taxa: 'taxa',
    formBairro: 'Jardim Jordão Residencial'
},{
    nomeBairro: 'Outros bairros (a taxa será informada no final)',
    valor: 0,
    taxa: 'taxa',
    formBairro: 'Outros bairros'
}]

const generateDomDistrict = (bairro) => {
    const bairroEl = document.createElement('tr')
    const inputH = document.createElement('input')
    const tdGeral = document.createElement('td')
    const tdBairro = document.createElement('td')
    const tdValor = document.createElement('td')
    const tdBtn = document.createElement('td')
    const btn = document.createElement('button')

    bairroEl.appendChild(tdGeral)
    tdGeral.appendChild(inputH)
    inputH.setAttribute('type', 'hidden')
    inputH.setAttribute('value', '1')

    bairroEl.appendChild(tdBairro)
    tdBairro.textContent = bairro.nomeBairro

    bairroEl.appendChild(tdValor)
    tdValor.textContent = 'R$ ' + bairro.valor.toFixed(2).replace('.', ',')

    bairroEl.appendChild(tdBtn)
    btn.textContent = 'ESCOLHER'
    tdBtn.appendChild(btn)

    btn.addEventListener('click', (e) => {
        e.preventDefault()
        if(bairro.nomeBairro === 'Retirar na Moraes Lanches' || bairro.nomeBairro === 'Outros bairros (a taxa será informada no final)') {
            criaPedidos(parseFloat(inputH.value), bairro.nomeBairro, bairro.valor, bairro.taxa, bairro.formBairro)
        } else{
            criaPedidos(parseFloat(inputH.value), ('TAXA DE ENTREGA ' + bairro.nomeBairro), bairro.valor, bairro.taxa, bairro.formBairro)
        }

        location.assign('./confirma.html')
    })

    return bairroEl

}

const addBairrosTela = () => {
    bairrosLista.forEach((bairro) => {
        document.querySelector('table').appendChild(generateDomDistrict(bairro))
    })
}

export {addTblEl, addBairrosTela, generateDomDistrict, pulaTaxa}