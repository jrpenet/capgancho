import {criaPedidosExclui, apagaPedidosExclui} from './pedidosExclui'
import {criaCadastro, getCadastro} from './cadastro'
import {getPedidos, criaPedidos} from './pedidos'

if(getCadastro().length > 0){
    location.assign('./index.html')
}

const pegaoCep = document.querySelector('#localizacep')
    pegaoCep.addEventListener('input', () => {
    const url = 'https://viacep.com.br/ws/' + pegaoCep.value.replace('-', '') +  '/json/'
    fetch(url)
        .then((r) => r.json())
        .then(data => {
            if(data.logradouro == undefined || data.bairro == undefined || data.localidade  == undefined || data.uf  == undefined){
                alert('Endereço inválido')
                const rua = document.querySelector('#endereco')
                rua.value = ''
                const bairr = document.querySelector('#bairro')
                bairr.value = ''
                const city = document.querySelector('#cidade')
                city.value = ''
            }else{
                const rua = document.querySelector('#endereco')
                rua.value = data.logradouro
                const bairr = document.querySelector('#bairro')
                const opt = document.querySelector('#dinamico')
                opt.textContent = data.bairro
                opt.setAttribute('value', `${data.bairro}`)
                bairr.appendChild(opt)
                bairr.value = data.bairro
                const city = document.querySelector('#cidade')
                city.value = data.localidade + ' - ' + data.uf 
            }
            
        })
        
})

const botaoDeLoginIndex = document.querySelector('#btnLogin')
botaoDeLoginIndex.addEventListener('click', (e) => {
    e.preventDefault()

    const usuario = document.querySelector('#name').value
    const fone = document.querySelector('#newTel').value
    const cepUser = document.querySelector('#localizacep').value
    const address = document.querySelector('#endereco').value
    const n = document.querySelector('#nro').value
    const pRefe = document.querySelector('#rfr').value
    const bairroUsr = document.querySelector('#bairro').value
    const cityUsr = document.querySelector('#cidade').value
    const retLoja = document.querySelector('#retiraNaLoja').checked

    if(retLoja === true && usuario != '' && fone != '' && cepUser != '' && address != '' && n != '' && pRefe != '' && bairroUsr != ''){
        if(getPedidos().filter((x) => x.tipo === 'taxa') == 0 || (getPedidos().filter((x) => x.tipo === 'taxa').length > 0 && getPedidos().filter((x) => x.produto.includes('Retirar')).length < 1)){
            criaPedidos(1, 'Retirar na loja', 0, 'taxa', '', 'Delivery')
            apagaPedidosExclui()
        }
        
    }else{
        criaPedidosExclui('1')
    }

    if(getPedidos().length > 1 && usuario != '' && fone != '' && cepUser != '' && address != '' && n != '' && pRefe != '' && bairroUsr != ''){
        criaCadastro(usuario, fone, cepUser, address, n, pRefe, bairroUsr, cityUsr)
        location.assign('./confirma.html')
    }

    if(usuario != '' && fone != '' && cepUser != '' && address != '' && n != '' && pRefe != '' && bairroUsr != ''  && getPedidos().length < 2){
        criaCadastro(usuario, fone, cepUser, address, n, pRefe, bairroUsr, cityUsr)
    }
    
    if(usuario == '' || fone == '' || cepUser == '' || address == '' || n == '' || pRefe == '' || bairroUsr == ''  && getPedidos().length < 2){
        alert('Preencha todos os campos')
        
    }

    if(getPedidos().length < 2 && usuario != '' && fone != '' && cepUser != '' && address != '' && n != '' && pRefe != '' && bairroUsr != ''){
        location.assign('./index.html')
    }

    

        
})