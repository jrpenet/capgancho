let pedidosTemp = []

const loadPedidosTemp = function(){
    const pedidosTempJSON = sessionStorage.getItem('pedidosTemp')
    
    if(pedidosTempJSON !== null){
        return JSON.parse(pedidosTempJSON)
    } else {
        return []
    }
}

const savePedidosTemp = function(){
    sessionStorage.setItem('pedidosTemp', JSON.stringify(pedidosTemp))
}

//expose orders from module
const getPedidosTemp = () => pedidosTemp

const criaPedidosTemp = (select, hamb, precoProduto, tx, bairro, foto, descricao) =>{
    
    pedidosTemp.push({
        qtd: select,
        produto: hamb,
        preco: precoProduto,
        subt: select * precoProduto,
        tipo: tx,
        nomeDoBairro: bairro,
        img: foto,
        descr: descricao
    })
    savePedidosTemp()
}

const removePedidosTemp = (item) => {
    pedidosTemp.splice(item, 1)
    savePedidosTemp()
}

const apagaTemp = () => {
    sessionStorage.removeItem('pedidosTemp')
}

pedidosTemp = loadPedidosTemp()

export { getPedidosTemp, criaPedidosTemp, savePedidosTemp, removePedidosTemp, apagaTemp}