let cadastro = []

const loadCadastro = function(){
    const cadastroJSON = localStorage.getItem('cadastro')
    
    if(cadastroJSON !== null){
        return JSON.parse(cadastroJSON)
    } else {
        return []
    }
}

const saveCadastro = function(){
    localStorage.setItem('cadastro', JSON.stringify(cadastro))
}

//expose orders from module
const getCadastro = () => cadastro

const criaCadastro = (name, tel, cepRes, lograd, nr, refere, bairro, cidadeEsta) =>{
    
    cadastro.push({
        nome: name,
        cel: tel,
        cep: cepRes,
        rua: lograd,
        n: nr,
        pontoDeReferencia: refere,
        nomeDoBairro: bairro,
        city: cidadeEsta
    })
    saveCadastro()
}

const removeCadastro = (item) => {
    cadastro.splice(item, 1)
    saveCadastro()
}

const resetaCad = () => {
    localStorage.clear()
    location.assign('./home.html')
}

const mudaDados2 = (pgNEW, trocoNEW, obsNEW) => {
    cadastro.splice(1, 0, pgNEW)
    cadastro.splice(2, 0, trocoNEW)
    cadastro.splice(3, 0, obsNEW)
    cadastro.splice(4,3)
    saveCadastro()
}

cadastro = loadCadastro()

export { getCadastro, criaCadastro, saveCadastro, removeCadastro, resetaCad, mudaDados2}