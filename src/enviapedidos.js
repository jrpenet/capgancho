import {getPedidos} from './pedidos'
import {getCadastro} from './cadastro'

const usuarioNome = document.querySelector('#usuario')
usuarioNome.textContent = getCadastro()[0].nome.toUpperCase()

document.querySelector('.btnFim').addEventListener('click', (e) => {
    e.preventDefault()
    location.assign(`https://wa.me/558187194087?text=%2ANome%2A%3A%20${getCadastro()[0].nome}%0A%0A%2ACEP%2A%3A%20${getCadastro()[0].cep}%0A%0A%2AEndere%C3%A7o%2A%3A%20${getCadastro()[0].rua}%2C%20${getCadastro()[0].n}%0A%0A%2ABairro%2A%3A%20${getCadastro()[0].nomeDoBairro}%0A%0A%2ACelular%2A%3A%20${getCadastro()[0].cel}%0A%0A%2APonto%20de%20refer%C3%AAncia%2A%3A%20${getCadastro()[0].pontoDeReferencia}%0A%0A%2AForma%20de%20pagamento%2A%3A%20${getCadastro()[1]}%0A%0A%2ATroco%2A%3A%20${getCadastro()[2]}%0A%0A%2AObserva%C3%A7%C3%A3o%2A%3A%20${getCadastro()[3]}%0A%0A%2APEDIDO%2A%3A%20%0A${getPedidos().map((x) => [x.qtd, x.produto, 'R$ ' + x.subt.toFixed(2).replace('.', ',')].join(' ')).join(' -%0A')}%0A%0A%2AValor%20total%2A%3A%20R$${getPedidos().map((e) => e.subt).reduce((a, b) => {
        return a + b
    }, 0).toFixed(2).replace('.', ',')}`)
})