import {getPedidosTemp} from './temp'
import { criaPedidos } from './pedidos'

if(getPedidosTemp().length < 1){
    location.assign('./confirma.html')
}

const nomeProduto = document.querySelector('#nomePedido')
nomeProduto.textContent = getPedidosTemp()[0].produto

const mostraPedido = document.querySelector('#mostraPedido')
mostraPedido.setAttribute('src', `${getPedidosTemp()[0].img}`)

const descrProduto = document.querySelector('#descricaoDoPed')
descrProduto.textContent = getPedidosTemp()[0].descr

const obs = document.querySelector('#insereObs')

const confirmaPedido = document.querySelector('#confirmaInsere')
confirmaPedido.addEventListener('click', (e) => {
    e.preventDefault()

    if(obs.value !== ''){
        criaPedidos(getPedidosTemp()[0].qtd, getPedidosTemp()[0].produto + ' Obs: ' + obs.value, getPedidosTemp()[0].preco)
    }

    if(obs.value === ''){
        criaPedidos(getPedidosTemp()[0].qtd, getPedidosTemp()[0].produto, getPedidosTemp()[0].preco)
    }

    location.assign('./confirma.html')
})