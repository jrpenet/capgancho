import {criaPedidos} from './pedidos'
import {criaPedidosTemp} from './temp'

//gera o menu na tela

//cardapio da tela
const cardapio = [{
    nomeProduto: 'Crocodilo',
    descricao: 'Strogonoff de filé',
    preco: 59.9,
    foto: './images/crocodilo.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Canhão',
    descricao: 'Salame peperoni',
    preco: 59.9,
    foto: './images/canhao.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Terra do Nunca',
    descricao: 'Quatro queijos com palmito',
    preco: 59.9,
    foto: './images/terradonunca.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Boca do Fomo',
    descricao: 'Pimenta em pó, molho de pimenta, cebola, ovo, calabresa em lascas e tomate italiano',
    preco: 59.9,
    foto: './images/bocadofomo.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Ilha do Tesouro',
    descricao: 'Isca de picanha',
    preco: 59.9,
    foto: './images/ilhadotesouro.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Capitão Gancho',
    descricao: 'Presunto, azeitona verde, ovo, pimentão e cebola roxa',
    preco: 59.9,
    foto: './images/capganchi.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Caverna da Caveira',
    descricao: 'Frango, creme de leite, ervilha, palmito e bacon',
    preco: 59.9,
    foto: './images/cavernadacaveira.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Céu e Mar',
    descricao: 'Camarão com catupiry',
    preco: 59.9,
    foto: './images/ceuemar.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Coração de Jorge',
    descricao: 'Coração de galinha e milho',
    preco: 59.9,
    foto: './images/coracaodejorge.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Dente de ouro',
    descricao: 'Alho de óleo, tomate italiano, tempero verde e parmesão',
    preco: 59.9,
    foto: './images/dentedeouro.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Esqueleto',
    descricao: 'Brócolis, palmito, milho, tomate italiano e champignon',
    preco: 59.9,
    foto: './images/esqueleto.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Aventuras Mágicas',
    descricao: 'Filé, batata frita e catupiry',
    preco: 59.9,
    foto: './images/aventurasmagicas.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Guardião do Baú',
    descricao: 'Brócolis na manteiga e molho quatro queijos',
    preco: 59.9,
    foto: './images/guardiaodobau.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Moedas de Ouro',
    descricao: 'Milho e catupiry',
    preco: 59.9,
    foto: './images/moedasdeouro.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Moedas de Bronze',
    descricao: 'Calabresa',
    preco: 59.9,
    foto: './images/moedasdebronze.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Marujos',
    descricao: 'Cebola na manteiga',
    preco: 59.9,
    foto: './images/marujos.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Relógio Tic-Tac',
    descricao: 'Alho, bacon e tomate italiano',
    preco: 59.9,
    foto: './images/tictac.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Lágrimas de Crocodilo',
    descricao: 'Isca de picanha ao alho e óleo e tempero verde',
    preco: 59.9,
    foto: './images/lagrimasdecrocodilo.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Peter Pan',
    descricao: 'Filé e queijo coalho',
    preco: 59.9,
    foto: './images/peterpan.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Por Mil Crocodilos',
    descricao: 'Atum e cebola roxa',
    preco: 59.9,
    foto: './images/pormilcrocodilos.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Caveira de Prata',
    descricao: 'Linguiça, batata frita, ovos e catupiry',
    preco: 59.9,
    foto: './images/caveiradeprata.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Pássaro do Nunca',
    descricao: 'Suíno ao molho mostarda',
    preco: 59.9,
    foto: './images/passarodonunca.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Pó Mágico',
    descricao: 'Isca de costela ao molho mostarda e mel',
    preco: 59.9,
    foto: './images/pomagico.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Timão Dourado',
    descricao: 'Parmesão, mussarela, provolone, catupiry, gorgonzola e cheddar',
    preco: 59.9,
    foto: './images/timaodourado.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Capitão',
    descricao: 'Tomate seco e rúcula',
    preco: 59.9,
    foto: './images/capitao.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Pìzza do Capitão',
    descricao: 'Mussarela',
    preco: 59.9,
    foto: './images/pizzadocapitao.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Mar Azul',
    descricao: 'Filé de carangueijo',
    preco: 59.9,
    foto: './images/marazul.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Navio Pirata',
    descricao: 'Frango com catupiry',
    preco: 59.9,
    foto: './images/naviopirata.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Tesouro Perdido',
    descricao: 'Camarão e cream cheese',
    preco: 59.9,
    foto: './images/tesouroperdido.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Smity',
    descricao: 'Cebola caramelizada, orégano e queijo gorgonzola',
    preco: 59.9,
    foto: './images/smity.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Pirata Mau',
    descricao: 'Mussarela, filé em cubos, mostarda e orégano',
    preco: 59.9,
    foto: './images/piratamau.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Bússola',
    descricao: 'Mussarela, filé em cubos, mostarda e orégano',
    preco: 59.9,
    foto: './images/bussola.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Pirlipimpim',
    descricao: 'Bacalhau em lascas, cebola, azeitona preta e orégano',
    preco: 59.9,
    foto: './images/pirlimpimpim.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Jane Darling',
    descricao: 'Brócolis picado, requeijão e orégano',
    preco: 59.9,
    foto: './images/janedarling.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Navio Gancho',
    descricao: 'Mussarela, cebola roxa refogada na manteiga com rum e orégano',
    preco: 59.9,
    foto: './images/naviogancho.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Wendy',
    descricao: 'Mussarela, manga em lâminas rúcula e mel',
    preco: 59.9,
    foto: './images/wendy.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Árvore da Forca',
    descricao: 'Mussarela, dobe desfiado com hortelã e orégano',
    preco: 59.9,
    foto: './images/arvoredaforca.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Lanterna',
    descricao: 'Mussarela, champignon, brócolis, milho, tomate e orégano',
    preco: 59.9,
    foto: './images/lanterna.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Duende',
    descricao: 'Mussarela, escalope de filé, molho adeira e orégano',
    preco: 59.9,
    foto: './images/duende.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Jolly Roger',
    descricao: 'Mussarela, palmito picado, requeijão e óregano',
    preco: 59.9,
    foto: './images/jollyroger.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Rugby',
    descricao: 'Mussarela, filé cheddar',
    preco: 59.9,
    foto: './images/rugby.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Tooties o Menino',
    descricao: 'Mussarela e champignon',
    preco: 59.9,
    foto: './images/tooties.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Menina Perdida',
    descricao: 'Frango desfiado e bacon',
    preco: 59.9,
    foto: './images/meninaperdida.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Tripulação',
    descricao: 'Mussarela, bacon, milho e queijo cheddar',
    preco: 59.9,
    foto: './images/crew.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Tinker Bel',
    descricao: 'Charque desfiado e cebola roxa',
    preco: 59.9,
    foto: './images/tinkerbel.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Magia',
    descricao: 'Carne de sol, queijo e cebola',
    preco: 59.9,
    foto: './images/magia.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Pan',
    descricao: 'Carne de sol, queijo coalho, catupiry e cebola',
    preco: 59.9,
    foto: './images/pan.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Marguerita',
    descricao: 'Mussarela, tomate em rodela, manjericão e parmesão',
    preco: 59.9,
    foto: './images/marguerita.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Fruto do Mar',
    descricao: 'Mussarela, bacalhau, carangueijo, camarão e pimenta colorido',
    preco: 59.9,
    foto: './images/frutosdomar.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Tropical',
    descricao: 'Mussarela, manga, uva, abacaxi, morango, banana e mel',
    preco: 59.9,
    foto: './images/tropical.jpeg',
    tipo: 'pizza'
},{
    nomeProduto: 'Céu Azul',
    descricao: 'Ganache preto, negresco e creme de leite',
    preco: 59.9,
    foto: './images/ceuazul.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Coração de Mary',
    descricao: 'Creme de brigadeiro preto e chocolate granulado',
    preco: 59.9,
    foto: './images/coracaodemary.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Asas de Fada',
    descricao: 'Ganache branco, bombom sonho de valsa e serenata de amor',
    preco: 59.9,
    foto: './images/asasdefada.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Sininho',
    descricao: 'Doce de leite, leite condensado, chocolate branco',
    preco: 59.9,
    foto: './images/sininho.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Mundo das fadas',
    descricao: 'Mussarela, goiabada, leite condensado',
    preco: 59.9,
    foto: './images/mundodasfadas.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Encantamento',
    descricao: 'Doce de leite, banana laminada com caramelo e côco',
    preco: 59.9,
    foto: './images/encantamento.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Ponto de Luz',
    descricao: 'Ganache de chocolate e sorvete de creme',
    preco: 59.9,
    foto: './images/pontodeluz.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Profundezas do oceano',
    descricao: 'Ganache de chocolate e M&M',
    preco: 59.9,
    foto: './images/profundezas.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Mar Azul',
    descricao: 'Chocolate preto e bis',
    preco: 59.9,
    foto: './images/marazuldoce.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Pirata Fantasma',
    descricao: 'Nutella, banana e leite ninho',
    preco: 59.9,
    foto: './images/piratafantasma.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Aventura',
    descricao: 'Pizza de beijinho tostado',
    preco: 59.9,
    foto: './images/aventura.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Pó de Fada',
    descricao: 'Leite condensado, banana e canela',
    preco: 59.9,
    foto: './images/podefada.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Livro do Nunca',
    descricao: 'Ganache de chocolate preto, bolo pefit gateaun e sorvete',
    preco: 59.9,
    foto: './images/livrodonunca.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Maquiavélico',
    descricao: 'Leite condensado, ganache de chocolate, flocos de arroz e raspas de chocolate',
    preco: 59.9,
    foto: './images/maqui.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Leme',
    descricao: 'Leite condensado, ganache de chocolate, flocos de arroz e raspas de chocolate',
    preco: 59.9,
    foto: './images/leme.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Perna de Pau',
    descricao: 'Leite condensado, mussarela, banana coberta com açúcar e canela',
    preco: 59.9,
    foto: './images/pernadepau.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Sonho',
    descricao: 'Banana com doce de leite',
    preco: 59.9,
    foto: './images/sonho.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Reino Mágico',
    descricao: 'Ganache de chocolate branco, leite ninho e nutella',
    preco: 59.9,
    foto: './images/reinomagico.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Mistérios',
    descricao: 'Leite condensado, ganache de chocolate preto e côco',
    preco: 59.9,
    foto: './images/misterios.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Pixum',
    descricao: 'Leite condensado, creme de leite e bolo de rolo',
    preco: 59.9,
    foto: './images/pixum.jpeg',
    tipo: 'doce'
},{
    nomeProduto: 'Coca-cola',
    descricao: '350ml',
    preco: 6.9,
    foto: './images/',
    tipo: 'bebida'
},{
    nomeProduto: 'Coca-cola 2 litros',
    descricao: '2 litros',
    preco: 12,
    foto: './images/',
    tipo: 'bebida'
},{
    nomeProduto: 'Guaraná Antartica',
    descricao: '350ml',
    preco: 6.9,
    foto: './images/',
    tipo: 'bebida'
},{
    nomeProduto: 'Guaraná Kuat 2 litros',
    descricao: '2 litros',
    preco: 12,
    foto: './images/',
    tipo: 'bebida'
}]

const addElements = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container js-scroll')
    
    titulo.textContent = lanche.nomeProduto
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    divMain.appendChild(imagem)
    divMain.appendChild(select)
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.preco
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeProduto
        const price = lanche.preco
        const image = lanche.foto
        const d = lanche.descricao

        if(lanche.tipo === 'bebida'){
            criaPedidos(qt, prodt, price)
            location.assign('./confirma.html')
        }else{
            criaPedidosTemp(qt, prodt, price, '','', image, d)
            location.assign('./conftemp.html')
        }
        
    })

    return divMain
}

const addCardapio = () => {
    cardapio.filter((x) => x.tipo === 'pizza').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#docTable').appendChild(hrEl)
        document.querySelector('#docTable').appendChild(addElements(lanche))
        
    })
}

const addCardapioDoce = () => {
    cardapio.filter((x) => x.tipo === 'doce').forEach((lanche) => {
        // const hrEl = document.createElement('hr')
        // document.querySelector('#docDoces').appendChild(hrEl)
        document.querySelector('#docDoces').appendChild(addElements(lanche))
    })
}

const addCardapioBebidas = () => {
    cardapio.filter((x) => x.tipo === 'bebida').forEach((lanche) => {
        // const hrEl = document.createElement('hr')
        // document.querySelector('#docDoces').appendChild(hrEl)
        document.querySelector('#docBebidas').appendChild(addElements(lanche))
    })
}


export {addCardapio, addElements, addCardapioDoce, addCardapioBebidas}