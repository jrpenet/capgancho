import {addCardapioBebidas} from './menuHam'

addCardapioBebidas()

//animacao ao scroll
const sections = document.querySelectorAll('.js-scroll')

function animaScroll() {
    sections.forEach((section) => {
        const sectionTop = section.getBoundingClientRect().top - (window.innerHeight * 0.75);
        if(sectionTop < 0){
            section.classList.add('ativo')
        }
        else{
            section.classList.remove('ativo')
        }
    })
}

animaScroll()

window.addEventListener('scroll', animaScroll)

document.querySelector('.goBack').addEventListener('click', (e) => {
    e.preventDefault()
    window.history.back()
})

