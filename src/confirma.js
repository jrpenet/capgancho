import {getPedidos, criaPedidos} from './pedidos'
import {getCadastro} from './cadastro'
import {criaPedidosExclui} from './pedidosExclui'
import {cartTable, insereCart, rodapeCart} from './cart'
import { apagaTemp } from './temp'

(function() {
    if( window.sessionStorage ) {
 
       if( !sessionStorage.getItem( 'firstLoad' ) ) {
          sessionStorage[ 'firstLoad' ] = true;
          window.location.reload();
 
       } else {
          sessionStorage.removeItem( 'firstLoad' );
       }
    }
 })(); 

let dt = new Date()
//let horas = dt.getHours()
console.log(dt.getHours())

// if(horas <= 17 || horas >= 23){
//     alert('cliente sera redirecionado')
// }
 
cartTable()
insereCart()
rodapeCart()
apagaTemp()

if(getPedidos().filter((x) => x.tipo === 'taxa').length < 1 && getCadastro().length > 0){
    //console.log('add bairro')
    const bairr = getCadastro().map((x) => x.nomeDoBairro)[0]
    if(bairr == 'Boa Vista' || bairr == 'Cohab' || bairr == 'Ilha do Leite' || bairr == 'Ilha do Retiro' || bairr == 'Parnamirim' || bairr == 'Graças' || bairr == 'Jaqueira' || bairr == 'Rosarinho' || bairr == 'Espinheiro'){
        criaPedidos(1, 'TAXA DE ENTREGA ' + bairr , 10, 'taxa', bairr, 'Delivery')
        criaPedidosExclui(1)
        location.reload()
    }else if(bairr == 'Piedade' || bairr == 'Pina' || bairr == 'Setubal' || bairr == 'Boa Viagem' || bairr == 'Brasília Teimosa' || bairr == 'Jardim Beira Rio'){
        criaPedidos(1, 'TAXA DE ENTREGA ' + bairr , 10, 'taxa', bairr, 'Delivery')
        criaPedidosExclui(1)
        location.reload()
    }else if(bairr != 'Piedade' && bairr != 'Setubal' && bairr != 'Boa Viagem' && bairr != 'Brasília Teimosa' && bairr != 'Jardim Beira Rio' && bairr != 'Cabanga' && bairr != 'Ilha do Leite' && bairr != 'Ilha do Retiro' && bairr != 'Parnamirim' && bairr != 'Graças' && bairr != 'Jaqueira' && bairr != 'Rosarinho' && bairr != 'Espinheiro' && bairr != 'Boa Vista'){
        criaPedidos(1, 'Outros bairros - a taxa será informada no final' + '(' + bairr + ')', 0, 'taxa', 'Outros Bairros', 'Delivery')
        criaPedidosExclui(1)
        location.reload()
    }
}

document.querySelector('.btn-comprarMais').addEventListener('click', (e) => {
    e.preventDefault()
    location.assign('./index.html')
})

document.querySelector('#finalizarPedido').addEventListener('click', (e) => {
    e.preventDefault()
    if(getPedidos().map((x)=> x.tipo).includes('taxa')){
        location.assign('./forms.html')
    }else{
        location.assign('./txentrega.html')
    }
})