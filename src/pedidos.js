let pedidos = []

const loadPedidos = function(){
    const pedidosJSON = sessionStorage.getItem('pedidos')
    
    if(pedidosJSON !== null){
        return JSON.parse(pedidosJSON)
    } else {
        return []
    }
}

const savePedidos = function(){
    sessionStorage.setItem('pedidos', JSON.stringify(pedidos))
}

//expose orders from module
const getPedidos = () => pedidos

const criaPedidos = (select, hamb, precoProduto, tx, bairro) =>{
    
    pedidos.push({
        qtd: select,
        produto: hamb,
        preco: precoProduto,
        subt: select * precoProduto,
        tipo: tx,
        nomeDoBairro: bairro
    })
    savePedidos()
}

const removePedidos = (item) => {
    pedidos.splice(item, 1)
    savePedidos()
}

pedidos = loadPedidos()

export { getPedidos, criaPedidos, savePedidos, removePedidos}