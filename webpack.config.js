const path = require('path')

module.exports = {
    entry: {
        home: ['babel-polyfill', './src/home.js'],
        index: ['babel-polyfill', './src/index.js'],
        cardapio: ['babel-polyfill', './src/cardapio.js'],
        doces: ['babel-polyfill', './src/doces.js'],
        beverage: ['babel-polyfill', './src/beverage.js'],
        conftemp: ['babel-polyfill', './src/conftemp.js'],
        confirma: ['babel-polyfill', './src/confirma.js'],
        forms: ['babel-polyfill', './src/forms.js'],
        enviapedidos: ['babel-polyfill', './src/enviapedidos.js']

    },
    output: {
        path: path.resolve(__dirname, 'public/scripts'),
        filename: '[name]-bundle.js'
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['env']
                }
            }
        }]
    },
    devServer: {
        contentBase: path.resolve(__dirname, 'public'),
        publicPath: '/scripts/'
    },
    devtool: 'source-map'
}